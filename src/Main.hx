/**
 * Haxe PEG テスト by あるる（きのもと 結衣）@arlez80
 **/

import ja.arlez80.hxpeg.*;

class Main {
	static function test_instance() {
		var binop = ( root:Array<Dynamic>, group:Array<Dynamic> ) -> { "op": group[0], "left": root[0], "right": group[1] };
		var number = new PegCapture( new PegRegex( "[0-9]+" ), ( s:String ) -> { "number": Std.parseInt( s ) } );
		var term = new PegCaptureFolding(
			new PegConcat([
				number,
				new PegGreedy(
					new PegCaptureGroup(
						new PegConcat([
							new PegCapture(
								new PegStringSelect( "*/%" )
							),
							number
						])
					),
					0
				)
			]),
			binop
		);
		var expr = new PegCaptureFolding(
			new PegConcat([
				term,
				new PegGreedy(
					new PegCaptureGroup(
						new PegConcat([
							new PegCapture(
								new PegStringSelect( "+-" )
							),
							term
						])
					),
					0
				)
			]),
			binop
		);
		var parser = expr;
		var result = parser.parse( "1+2+3*4+5" );

		trace( result.accept );
		trace( result.capture );
	}

	static function test_peg() {
		var binop_non_folding = function ( group:Array<Dynamic> ):Dynamic {
			var node:Dynamic = group[0];
			var i:Int = 1;
			while( i < group.length ) {
				node = { "op": group[i+0], "left": node, "right": group[i+1] };
				i += 2;
			}
			return node;
		};
		var number = ( s:Dynamic ) -> { "number": Std.parseInt( s ) };

		var parser = Peg.generate('
			expr < term ( ~"[\\+\\-]" term )*
			term < factor ( ~"[\\*/]" factor )*
			factor < number / "(" expr ")"
			number <~ ~"[0-9]+"
		', [
			'expr' => binop_non_folding,
			'term' => binop_non_folding,
			'number' => number,
		]);
		var result = parser.parse( "1+2+3*4+5" );

		trace( result.accept );
		trace( result.capture );
	}

	static function main() {
		test_instance( );
		test_peg( );
	}
}
