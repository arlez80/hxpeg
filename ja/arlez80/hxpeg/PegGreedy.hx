/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegGreedy extends PegTree
{
	public final a:PegTree;
	public final least:Int;
	public final length:Int;

	public function new( _a:PegTree, _least:Int = 0, _length:Int = -1 )
	{
		this.a = _a;
		this.least = _least;
		this.length = _length;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		var total_length:Int = 0;
		var total_capture:Array<Dynamic> = [];
		var count:Int = 0;

		while( true ) {
			var ra:PegResult = this.a.parse( buffer, p );
			if( !ra.accept ) {
				break;
			}

			p += ra.length;
			total_length += ra.length;

			if( ra.group ) {
				total_capture.push( ra.capture );
			}else {
				total_capture = total_capture.concat( ra.capture );
			}

			count ++;
			if( this.length != -1 && this.length <= count ) {
				break;
			}
		}

		if( this.least <= count ) {
			return new PegResult( true, total_length, total_capture );
		}else {
			return new PegResult( false );
		}
	}
}
