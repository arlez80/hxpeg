/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegAheadNotAccept extends PegTree
{
	public final a:PegTree;

	public function new( _a:PegTree )
	{
		this.a = _a;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		return new PegResult( ! this.a.parse( buffer, p ).accept, 0 );
	}
}
