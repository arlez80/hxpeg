/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegCaptureFunction extends PegTree
{
	public final a:PegTree;
	public final f:Array<Dynamic>->Dynamic;

	public function new( _a:PegTree, _f:( captured:Array<Dynamic> )->Dynamic = null )
	{
		this.a = _a;
		this.f = _f;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		var ra:PegResult = a.parse( buffer, p );
		if( ra.accept ) {
			if( this.f != null ) {
				ra.capture = [ this.f( ra.capture ) ];
			}
			return ra;
		}

		return new PegResult( false );
	}
}
