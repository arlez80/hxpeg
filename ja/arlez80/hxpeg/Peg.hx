/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

enum PegGenerator
{
	name( name:String );
	literal( literal:String );
	regex( pattern:String, option:String );
	reg_range( pattern:String );
	any;
	debug;
	suffix( child:PegGenerator, suffix:String );
	prefix( child:PegGenerator, prefix:String );
	concat( child:Array<PegGenerator> );
	select( child:Array<PegGenerator> );
	define( name:String, arrow:String, expr:PegGenerator );
}

class PegGenerateError
{
	public var message:String = "";

	public function new( _mes:String )
	{
		this.message = _mes;
	}
}

class Peg
{
	macro static public function macro_parse( src:String )
	{
		return macro $v{ Peg.parse( src ) };
	}

	static public function parse( src:String ):Array<PegGenerator>
	{
		var p_expr = new PegSelect([ null ]);
		// Blank
		var p_eol = new PegRegex( "[\r\n]" );
		var p_comment = new PegRegex( "#[^\n\r]*", "u" );
		var p_space = new PegGreedy( new PegSelect([
			new PegLiteral( " " )
		,	new PegLiteral( "\t" )
		,	p_eol
		,	p_comment
		]));
		// Terminators
		var p_name = new PegConcat([
			new PegCapture( new PegRegex( "[A-Za-z_][A-Za-z0-9_]+" ), function( name:String ):Dynamic {
				return PegGenerator.name( name );
			})
		,	p_space
		]);
		var p_literal = new PegCaptureFunction(
			new PegConcat([
				new PegSelect([
					new PegConcat([
						new PegLiteral( "\"" )
					,	new PegCapture( new PegRegex( "(\\\\.|[^\"\\\\])*" ) )
					,	new PegLiteral( "\"" )
					])
				,	new PegConcat([
						new PegLiteral( "\'" )
					,	new PegCapture( new PegRegex( "(\\\\.|[^'\\\\])*" ) )
					,	new PegLiteral( "\'" )
					])
				])
			,	p_space
			])
		,	function( captures:Array<Dynamic> ):Dynamic {
				return PegGenerator.literal( Std.string( captures[0] ) );
			}
		);
		var p_regex = new PegCaptureFunction(
			new PegConcat([
				new PegLiteral( "~" )
			,	new PegSelect([
					new PegConcat([
						new PegLiteral( "\"" )
					,	new PegCapture( new PegRegex( "(\\\\.|[^\"\\\\])*" ) )
					,	new PegLiteral( "\"" )
					])
				,	new PegConcat([
						new PegLiteral( "\'" )
					,	new PegCapture( new PegRegex( "(\\\\.|[^\'\\\\])*" ) )
					,	new PegLiteral( "\'" )
					])
				])
			,	new PegCapture( new PegRegex( "[A-Za-z]*" ) )
			,	p_space
			])
		,	function( captures:Array<Dynamic> ):Dynamic {
				return PegGenerator.regex( Std.string( captures[0] ), Std.string( captures[1] ) );
			}
		);
		var p_range = new PegCaptureFunction(
			new PegConcat([
				new PegLiteral( "[" )
			,	new PegCapture( new PegRegex( "(\\\\.|[^\\]\\\\])*" ) )
			,	new PegLiteral( "]" )
			,	p_space
			])
		,	function( captures:Array<Dynamic> ):Dynamic {
				return PegGenerator.reg_range( Std.string( captures[0] ) );
			}
		);
		var p_any = new PegCapture(
			new PegConcat([ new PegLiteral( "." ), p_space ])
		,	function( dot:String ):Dynamic { return PegGenerator.any; }
		);
		var p_debug = new PegCapture(
			new PegConcat([ new PegLiteral( "@" ), p_space ])
		,	function( at:String ):Dynamic { return PegGenerator.debug; }
		);

		// Operators
		var p_arrow = new PegConcat([
			new PegCapture( 
				new PegSelect([
					new PegLiteral( "=" )
				,	new PegLiteral( "<~" )
				,	new PegLiteral( "<-" )
				,	new PegLiteral( "<:" )
				,	new PegLiteral( "<" )
				])
			)
		,	p_space
		]);
		var p_select = new PegConcat([ new PegLiteral( "/" ), p_space ]);
		var p_lookahead = new PegConcat([ new PegCapture( new PegLiteral( "&" ) ), p_space ]);
		var p_not = new PegConcat([ new PegCapture( new PegLiteral( "!" ) ), p_space ]);
		var p_option = new PegConcat([ new PegCapture( new PegLiteral( "?" ) ), p_space ]);
		var p_one_or_more = new PegConcat([ new PegCapture( new PegLiteral( "+" ) ), p_space ]);
		var p_zero_or_more = new PegConcat([ new PegCapture( new PegLiteral( "*" ) ), p_space ]);
		var p_factor = new PegConcat([
			new PegLiteral( "(" )
		,	p_space
		,	p_expr
		,	new PegLiteral( ")" )
		,	p_space
		]);

		// Syntaxes
		var p_primary = new PegSelect([
			new PegConcat([ p_name, new PegAheadNotAccept( p_arrow ) ])
		,	p_factor
		,	p_literal
		,	p_regex
		,	p_range
		,	p_debug
		,	p_any
		]);
		var p_suffix = new PegCaptureFunction(
			new PegConcat([
				p_primary
			,	new PegGreedy(
					new PegSelect([
						p_option
					,	p_one_or_more
					,	p_zero_or_more
					]),
					0, 1
				)
			,	p_space
			])
		,	function( captures:Array<Dynamic> ):Dynamic {
				if( captures.length == 2 ) {
					return PegGenerator.suffix( captures[0], Std.string( captures[1] ) );
				}
				return captures[0];
			}
		);
		var p_prefix = new PegCaptureFunction(
			new PegConcat([
				new PegGreedy(
					new PegSelect([
						p_lookahead
					,	p_not
					]),
					0, 1
				),
				p_suffix
			])
		,	function( captures:Array<Dynamic> ):Dynamic {
				if( captures.length == 2 ) {
					return PegGenerator.prefix( captures[1], Std.string( captures[0] ) );
				}
				return captures[0];
			}
		);
		var p_concat = new PegCaptureFunction(
			new PegGreedy(
				new PegConcat([
					new PegAheadNotAccept( p_select )
				,	p_prefix
				])
			,	1
			)
		,	function( captures:Array<Dynamic> ):Dynamic {
				return PegGenerator.concat( cast captures );
			}
		);
		p_expr.a[0] = new PegCaptureFunction(
			new PegConcat([
				p_concat
			,	new PegGreedy(
					new PegConcat([
						p_select
					,	p_concat
					])
				)
			])
		,	function( captures:Array<Dynamic> ):Dynamic {
				return PegGenerator.select( cast captures );
			}
		);
		var p_define = new PegCaptureFunction(
			new PegConcat([
				p_name
			,	p_arrow
			,	p_expr
			,	p_space
			])
		,	function( captures:Array<Dynamic> ):Dynamic {
				var name:String = switch( captures[0] ) {
					case PegGenerator.name( _name ): _name;
					default: "error";
				};
				return PegGenerator.define( name, Std.string( captures[1] ), captures[2] );
			}
		);
		var root = new PegConcat([
			p_space
		,	new PegGreedy( p_define, 1 )
		,	p_space
		,	new PegAheadNotAccept( new PegAny( ) )
		]);
		
		var result = root.parse( src, 0 );
		if( !result.accept ) {
			return null;
		}

		return cast result.capture;
	}

	static public function applyFunctions( result:Array<PegGenerator>, capture_functions:Map<String,Dynamic->Dynamic> = null )
	{
		// 名前定義
		var labels:Map<String, PegTree> = [];
		var first_label_name:String = "";

		for( t in result ) {
			switch( t ) {
				case PegGenerator.define( _name, _, _ ):
					labels.set( _name, new PegSelect([ null ]) );
					if( first_label_name == "" ) {
						first_label_name = _name;
					}
				default:
					throw new PegGenerateError( "構文エラー" );
			}
		}
		for( t in result ) {
			switch( t ) {
				case PegGenerator.define( _name, _, _ ):
					var label:PegSelect = cast labels[_name];
					label.a[0] = Peg.compile( labels, t, capture_functions );
				default:
			}
		}

		return labels[first_label_name];
	}

	static private function compile( labels:Map<String,PegTree>, node:PegGenerator, capture_functions:Map<String,Dynamic->Dynamic> = null ):PegTree
	{
		return switch( node ) {
			case name( _name ):
				if( ! labels.exists( _name ) ) {
					throw new PegGenerateError( _name + "は未定義です。" );
				}
				labels[_name];
			case literal( _literal ):
				new PegCapture( new PegLiteral( _literal ) );
			case regex( pattern, option ):
				new PegCapture( new PegRegex( pattern, option ) );
			case reg_range( pattern ):
				new PegCapture( new PegRegex( "[" + pattern + "]", "" ) );
			case any:
				new PegCapture( new PegAny( ) );
			case debug:
				new PegDebug( "", true );
			case suffix( _child, _suffix ):
				var tree_child = Peg.compile( labels, _child );
				switch( _suffix ) {
					case "*": new PegGreedy( tree_child );
					case "+": new PegGreedy( tree_child, 1 );
					case "?": new PegGreedy( tree_child, 0, 1 );
					default:
						throw "Unknown suffix " + _suffix;
				};
			case prefix( _child, _prefix ):
				var tree_child = Peg.compile( labels, _child );
				switch( _prefix ) {
					case "&": new PegAheadAccept( tree_child );
					case "!": new PegAheadNotAccept( tree_child );
					default:
						throw "Unknown prefix " + _prefix;
				};
			case concat( _child ):
				if( 2 <= _child.length ) {
					var v:Array<PegTree> = [];
					for( t in _child ) {
						v.push( Peg.compile( labels, t ) );
					}
					new PegConcat( v );
				}else {
					Peg.compile( labels, _child[0] );
				};
			case select( _child ):
				if( 2 <= _child.length ) {
					var v:Array<PegTree> = [];
					for( t in _child ) {
						v.push( Peg.compile( labels, t ) );
					}
					new PegSelect( v );
				}else {
					Peg.compile( labels, _child[0] );
				};
			case define( _name, _arrow, _expr ):
				var tree = Peg.compile( labels, _expr );
				switch( _arrow ) {
					case "<", "=":
						if( capture_functions != null ) {
							if( capture_functions.exists( _name ) ) {
								tree = new PegCaptureFunction( tree, capture_functions[_name] );
							}
						}
					case "<~":
						if( capture_functions != null ) {
							if( capture_functions.exists( _name ) ) {
								tree = new PegCapture( tree, capture_functions[_name] );
							}
						}
					case "<-":
						tree = new PegCaptureRemove( tree );
					case "<:":
						tree = new PegCaptureState( tree, cast capture_functions[_name] );
				}

				tree;
		};
	}

	static public function generate( src:String, capture_functions:Map<String,Dynamic->Dynamic> = null ):PegTree
	{
		var result = Peg.parse( src );
		if( result == null ) {
			return null;
		}

		return Peg.applyFunctions( result, capture_functions );
	}
}
