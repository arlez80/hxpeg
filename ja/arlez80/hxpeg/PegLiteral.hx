/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegLiteral extends PegTree
{
	public final literal:String;
	public final literal_length:Int;

	public function new( _literal:String )
	{
		this.literal = _literal;
		this.literal_length = this.literal.length;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		if( buffer.substr( p, this.literal_length ) == this.literal ) {
			return new PegResult( true, this.literal_length );
		}else {
			return new PegResult( false );
		}
	}
}
