/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegCapture extends PegTree
{
	public final a:PegTree;
	public final f:String->Dynamic;

	public function new( _a:PegTree, _f:( captured:String )->Dynamic = null )
	{
		this.a = _a;
		this.f = _f;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		var ra:PegResult = a.parse( buffer, p );
		if( ra.accept ) {
			var s:String = buffer.substr( p, ra.length );
			if( this.f != null ) {
				ra.capture = [ this.f( s ) ];
			}else {
				ra.capture = [ s ];
			}
			return ra;
		}
		return new PegResult( false );
	}
}
