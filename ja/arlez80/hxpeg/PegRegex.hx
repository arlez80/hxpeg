/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegRegex extends PegTree
{
	public final regex:EReg;

	public function new( r:String, option:String = "" )
	{
		this.regex = new EReg( r, option );
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		// XXX: matchSubはUTF8が入っているダメ？
		if( this.regex.match( buffer.substr( p ) ) ) {
			if( this.regex.matchedPos( ).pos == 0 ) {
				return new PegResult( true, this.regex.matchedPos( ).len );
			}else {
				return new PegResult( false );
			}
		}else {
			return new PegResult( false );
		}
	}
}
