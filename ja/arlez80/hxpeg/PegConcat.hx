/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegConcat extends PegTree
{
	public final a:Array<PegTree>;

	public function new( _a:Array<PegTree> )
	{
		this.a = _a;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		var total_length:Int = 0;
		var total_capture:Array<Dynamic> = [];

		for( t in this.a ) {
			var ra:PegResult = t.parse( buffer, p );
			if( !ra.accept ) {
				return new PegResult( false );
			}

			p += ra.length;
			total_length += ra.length;

			if( ra.group ) {
				total_capture.push( ra.capture );
			}else {
				total_capture = total_capture.concat( ra.capture );
			}
		}

		return new PegResult( true, total_length, total_capture );
	}
}
