/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegSelect extends PegTree
{
	public final a:Array<PegTree>;

	public function new( _a:Array<PegTree> )
	{
		this.a = _a;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		for( t in this.a ) {
			var ra:PegResult = t.parse( buffer, p );
			if( ra.accept ) {
				return ra;
			}
		}

		return new PegResult( false );
	}
}
