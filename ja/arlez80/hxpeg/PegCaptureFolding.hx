/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegCaptureFolding extends PegTree
{
	public final a:PegTree;
	public final f:Array<Dynamic>->Array<Dynamic>->Dynamic;

	public function new( _a:PegTree, _f:( captured:Array<Dynamic>, group:Array<Dynamic> )->Dynamic = null )
	{
		this.a = _a;
		this.f = _f;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		var ra:PegResult = a.parse( buffer, p );
		if( ra.accept ) {
			if( 0 < ra.capture.length ) {
				var result:Array<Dynamic> = [ra.capture[0]];
				for( i in 1 ... ra.capture.length ) {
					result = [ this.f( result, ra.capture[i] ) ];
				}
				ra.capture = result;
			}
			return ra;
		}

		return new PegResult( false );
	}
}
