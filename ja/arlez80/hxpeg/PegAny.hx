/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegAny extends PegTree
{
	public function new( )
	{
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		if( p < buffer.length ) {
			return new PegResult( true, 1 );
		}else {
			return new PegResult( false );
		}
	}
}
