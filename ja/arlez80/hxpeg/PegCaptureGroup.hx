/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegCaptureGroup extends PegTree
{
	public final a:PegTree;

	public function new( _a:PegTree )
	{
		this.a = _a;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		var ra:PegResult = a.parse( buffer, p );
		if( ra.accept ) {
			ra.group = true;
			return ra;
		}
		return new PegResult( false );
	}
}
