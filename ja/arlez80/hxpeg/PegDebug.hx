/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegDebug extends PegTree
{
	public final message:String;
	public final accept:Bool;

	public function new( _message:String, _accept:Bool = true )
	{
		this.message = _message;
		this.accept = _accept;
	}

	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		trace( "* peg debug *" );
		trace( "pos: " + p );
		trace( "message: " + this.message );
		trace( "code: " + buffer.charCodeAt( p ) );
		trace( "char: " + buffer.charAt( p ) );
		return new PegResult( this.accept );
	}
}
