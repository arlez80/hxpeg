/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegCaptureState extends PegTree
{
	public final a:PegTree;
	public final f:(args:{buffer:String, p:Int, result:PegResult})->Dynamic;

	public function new( _a:PegTree, _f:( args:{buffer:String, p:Int, result:PegResult} )->Dynamic = null )
	{
		this.a = _a;
		this.f = _f;
	}
 
	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		var ra:PegResult = a.parse( buffer, p );
		if( this.f != null ) {
			this.f( {buffer:buffer, p:p, result:ra} );
		}
		return ra;
	}
}
