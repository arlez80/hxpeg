/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
 package ja.arlez80.hxpeg;

class PegStringSelect extends PegTree
{
	public final chars:Array<String>;
 
	public function new( _s:String )
	{
		 this.chars = _s.split( "" );
	}
 
	override public function parse( buffer:String, p:Int = 0 ):PegResult
	{
		for( c in this.chars ) {
			var len:Int = c.length;
			if( buffer.substr( p, len ) == c ) {
				return new PegResult( true, len );
			}
		}

		return new PegResult( false );
	}
}
 