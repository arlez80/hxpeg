/**
 * Haxe Parsing Expression Grammar
 * @author	あるる（きのもと 結衣） @arlez80
 */
package ja.arlez80.hxpeg;

class PegResult
{
	public final accept:Bool;
	public final length:Int;
	public var capture:Array<Dynamic>;
	public var group:Bool;

	public function new( _accept:Bool, _length:Int = 0, _capture:Array<Dynamic> = null )
	{
		this.accept = _accept;
		this.length = _length;
		if( _capture != null ) {
			this.capture = _capture;
		}else {
			this.capture = [];
		}
		this.group = false;
	}
}
